<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Infomation;
use App\Models\ListMenuPost;
use App\Models\Post;
use App\Models\Produce;
use App\Models\Support;
use App\Models\Slide;
use App\Models\PostProduce;
use Auth;

class HomeController extends Controller
{
    public function getHome()
    {
        $data_info = Infomation::find(1);

        $form_data = [
            'title' => [
                'title' => 'Tiêu đề trang',
                'name' => 'title',
                'mo_ta' => '',
                'value' => $data_info['title'],
                'tag' => 'input'
            ],
            'description' => [
                'title' => 'Mô tả trang',
                'name' => 'description',
                'mo_ta' => '',
                'value' => $data_info['description'],
                'tag' => 'input'
            ],
            'keywords' => [
                'title' => 'Từ khóa tìm kiếm',
                'name' => 'keywords',
                'mo_ta' => 'Mỗi từ khóa cách nhau bằng dấu phẩy',
                'value' => $data_info['keywords'],
                'tag' => 'input'
            ],
            'address' => [
                'title' => 'Địa chỉ cửa hàng',
                'name' => 'address',
                'mo_ta' => '',
                'value' => $data_info['address'],
                'tag' => 'input'
            ],
            'coordinate' => [
                'title' => 'Mã nhúng vị trí',
                'name' => 'coordinate',
                'mo_ta' => '',
                'value' => $data_info['coordinate'],
                'tag' => 'input'
            ],
            'email' => [
                'title' => 'Địa chỉ Email của bạn',
                'name' => 'email',
                'mo_ta' => '',
                'type' => 'email',
                'value' => $data_info['email'],
                'tag' => 'input'
            ],
            'phone_number' => [
                'title' => 'Số điện thoại của bạn',
                'name' => 'phone_number',
                'mo_ta' => '',
                'value' => $data_info['phone_number'],
                'tag' => 'input'
            ],
            'link_post_index' => [
                'title' => 'Đường dẫn bài viết trang chủ',
                'name' => 'link_post_index',
                'mo_ta' => '',
                'value' => $data_info['link_post_index'],
                'tag' => 'input'
            ],
            'banner' => [
                'title' => 'Hình ảnh banner đầu trang',
                'name' => 'banner',
                'mo_ta' => 'Tải lên hình ảnh có kích thước 1920x221. Bạn có thể tải xuống ảnh mẫu tại <a href="/img/banner/pop-banner.jpg">đây</a>',
                'value' => $data_info['banner'],
                'tag' => 'input',
                'type' => 'file'
            ],
        ];

        return view('auth.form', [
            'form' => $form_data,
            'title' => 'CÀI ĐẶT THÔNG TIN CƠ BẢN',
            'route_form' => 'admin.manager.index'
        ]);
    }
    public function actionEditInfomation(Request $request) 
    {
        $this->validate($request, [
            
        ]);

        $value_data = Infomation::find(1);
        unset($request['_token']);
        $value_data->fill($request->all());
        
        if($request->hasFile('banner')) {
            $value_data->banner = $this->save_img($request->banner);
        }
        $value_data->save();

        return redirect()->back()->with('thong_bao', 'Sửa thành công thông tin website');
    }

    public function getListPostMenu()
    {
        $data_list = ListMenuPost::all();

        return view('auth.table_menu', [
            'title' => 'NỘI DUNG THANH MENU',
            'route_create' => 'admin.manager.create_list_post_menu',
            'route_remove' => 'admin.manager.remove_list_post_menu',
            'data_route_edit' => 'id',
            'data' => $data_list,
            'table_colum' => [
                [
                    'name' => 'Tên Hiển Thị',
                    'id' => 'name'
                ],
                [
                    'name' => 'Mô Tả',
                    'id' => 'description'
                ],
                [
                    'name' => 'Liên Kết',
                    'id' => 'route_name'
                ]
            ]
        ]);
    }

    public function getSlides()
    {
        $data_list = Slide::all();

        return view('auth.table_menu', [
            'title' => 'NỘI DUNG THANH MENU',
            'route_create' => 'admin.manager.create_slides',
            'route_remove' => 'admin.manager.remove_slides',
            'data_route_edit' => 'id',
            'data' => $data_list,
            'table_colum' => [
                [
                    'name' => 'Hình Ảnh Hiển Thị',
                    'id' => 'src',
                    'type' => 'file',
                    'mo_ta' => 'Sử dụng hình ảnh có kích thước 899x409 để tránh bị vỡ. Hình ảnh mẫu tại <a href="/img/slider/2.jpg">đây</a>'
                ]
            ]
        ]);
    }
    public function actionCreateSlides(Request $request)
    {
        $this->validate($request, [
            'src' => 'required|file',
        ]);
        
        $slide = new Slide;
        
        if($request->hasFile('src')) {
            $slide->src = $this->save_img($request->src);
        }
        
        $slide->save();
        
        return redirect()->back()->with('thong_bao', 'Tải lên thành công hình ảnh');
    }
    
    public function getPosts()
    {
        $data_list = Post::all();

        return view('auth.table_menu', [
            'title' => 'TỔNG HỢP CÁC BÀI VIẾT',
            'route_create' => 'admin.manager.create_posts',
            'route_edit' => 'admin.manager.post.edit',
            'route_remove' => 'admin.manager.post.remove',
            'data_route_edit' => 'slug',
            'data' => $data_list,
            'table_colum' => [
                [
                    'name' => 'Tên Hiển Thị',
                    'id' => 'title'
                ],
                [
                    'name' => 'Hình Ảnh',
                    'id' => 'image',
                    'type' => 'file',
                    'mo_ta' => 'Nên sử dụng hình ảnh đại diện có kích thước chiều dài và chiều rộng bằng nhau ( Hình vuông )'
                ],
                [
                    'name' => 'Từ Khóa',
                    'id' => 'tags',
                    'mo_ta' => 'Các từ cách nhau bởi dấu phẩy'
                ],
                [
                    'name' => 'Mô Tả',
                    'id' => 'description'
                ],
                [
                    'name' => 'Liên Kết',
                    'id' => 'slug',
                    'mo_ta' => 'Viết không dấu và Các từ cách nhau bởi dấu "-"'
                ]
            ]
        ]);
    }

    public function actionCreatePosts(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'slug' => 'required',
            'tags' => 'required',
        ]);
        
        $post_new = new Post;
        
        if($request->hasFile('image')) {
            $post_new->image = $this->save_img($request->image);
        }
        $post_new->fill($request->all());

        $post_new->id_user = Auth::user()->id;
        $post_new->content = 'Chưa có nội dung';
        $post_new->save();
        
        return redirect()->route('admin.manager.post.edit', $post_new->slug)->with('thong_bao', 'Tạo thành công bài viết');
    }
    public function createListPostMenu(Request $request)
    { 
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'route_name' => 'required',
        ]);

        $data_new_menu = new ListMenuPost;

        $data_new_menu->fill($request->all());

        $data_new_menu->save();

        return redirect()->back()->with('thong_bao', 'Tạo thành công nội dung thanh menu');

    }

    public function getEditSlug($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $form_data = [
            'title' => [
                'title' => 'Tiêu đề bài viết',
                'name' => 'title',
                'mo_ta' => '',
                'value' => $post['title'],
                'tag' => 'input'
            ],
            'tags' => [
                'title' => 'Từ khóa của bài viết',
                'name' => 'tags',
                'mo_ta' => '',
                'value' => $post['tags'],
                'tag' => 'input'
            ],
            'description' => [
                'title' => 'Mô tả về bài viết',
                'name' => 'description',
                'mo_ta' => '',
                'value' => $post['description'],
                'tag' => 'input'
            ],
            'image' => [
                'title' => 'Hình Ảnh',
                'name' => 'image',
                'mo_ta' => 'Lưu ý: Nên lựa chọn ảnh có hình vuông để hình ảnh khi hiển thị không bị vỡ.',
                'value' => '',
                'type' => 'file',
                'tag' => 'input'
            ],
            'content' => [
                'title' => 'Nội dung bài viết',
                'name' => 'content',
                'mo_ta' => '',
                'value' => $post['content'],
                'tag' => 'textarea'
            ],
            
        ];
        return view('auth.form', [
            'form' => $form_data,
            'route_form' => 'admin.manager.post.edit',
            'route_data' => $post['slug'],
            'title' => 'CHỈNH SỬA: ' . $post->title
        ]);

    }

    public function actionEditSlug(Request $request, $slug)
    {
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'tags' => 'required',
        ]);
        $post = Post::where('slug', $slug)->first();

        $post->fill($request->all());

        if($request->hasFile('image')) {
            $post->image = $this->save_img($request->image);
        }
        $post->save();
        
        return redirect()->back()->with('thong_bao', 'Chỉnh sửa thành công bài viết');
    }
    public function actionRemove($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $post->delete();
        
        return redirect()->back()->with('thong_bao', 'Xóa thành công bài viết');
    }

    public function getSupport()
    {
        $data_list = Support::all();

        return view('auth.table_menu', [
            'title' => 'QUẢN LÝ TÀI KHOẢN MẠNG XÃ HỘI HỖ TRỢ ONLINE',
            'route_create' => 'admin.manager.create_support',
            'route_remove' => 'admin.manager.remove_support',
            'data_route_edit' => 'id',
            'data' => $data_list,
            'table_colum' => [
                [
                    'name' => 'Mạng Xã Hội',
                    'id' => 'social'
                ],
                [
                    'name' => 'Tên Hiển Thị',
                    'id' => 'name',
                ],
                [
                    'name' => 'Nội Dung',
                    'id' => 'href',
                ]
            ]
        ]);

        
    }
    public function createSupport(Request $request)
    {
        $this->validate($request, [
            'social' => 'required',
            'name' => 'required',
            'href' => 'required',
        ]);
        
        $support = new Support;

        $support->fill($request->all());

        $support->save();
        
        return redirect()->back()->with('thong_bao', 'Thêm mới thnafh công thông tin hỗ trợ');
    }

    public function actionRemoveSupport($id)
    {
        $support = Support::find($id);

        $support->delete();
        
        return redirect()->back()->with('thong_bao', 'Xóa thành công thông tin hỗ trợ');
    }
    public function actionRemoveSlidess($id)
    {
        $support = Slide::find($id);

        $support->delete();
        
        return redirect()->back()->with('thong_bao', 'Xóa thành công thông hình ảnh');
    }
    
    public function removeListPostMenu($id)
    {
        $data_remove = ListMenuPost::find($id);
        $data_remove->delete();

        return redirect()->back()->with('thong_bao', 'Xóa thành công nội dung thanh menu');
    }

    public function actionRemoveproduces($id)
    {
        $support = Produce::find($id);

        $support->delete();
        
        return redirect()->back()->with('thong_bao', 'Xóa thành công danh mục sản phẩm');
    }
    public function getProduces()
    {
        $data_list = Produce::all();

        return view('auth.table_menu', [
            'title' => 'QUẢN LÝ DANH MỤC SẢN PHẨM',
            'route_create' => 'admin.manager.create_produces',
            'route_remove' => 'admin.manager.remove_produces',
            'data_route_edit' => 'id',
            'data_view' => 'admin.manager.post_produces',
            'route_edit' => 'admin.manager.edit_produces',
            'data' => $data_list,
            'table_colum' => [
                [
                    'name' => 'Tên Danh Mục',
                    'id' => 'title'
                ],
                [
                    'name' => 'Hình Ảnh',
                    'id' => 'image',
                    'type' => 'file',
                    'mota' => 'Nên sử dụng hình ảnh có chiều dài bằng chiều rộng'
                ],
                [
                    'name' => 'Mô Tả',
                    'id' => 'description',
                ],
                [
                    'name' => 'Đường Dẫn',
                    'id' => 'slug',
                ],
                [
                    'name' => 'Từ Khóa',
                    'id' => 'tags',
                ]
            ]
        ]);
    }
    public function actionCreateProduces(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'slug' => 'required',
        ]);

        $produce = new Produce;

        if($request->hasFile('image')) {
            $produce->image = $this->save_img($request->image);
        }
        $produce->fill($request->all());
        $produce->id_user = Auth::user()->id;
        $produce->save();
        
        return redirect()->back()->with('thong_bao', 'Tạo thành công danh mục sản phẩm');
    }
    public function editProduces($id)
    {
        $produce = Produce::find($id);

        $form_data = [
            'title' => [
                'title' => 'Tên Danh Mục',
                'name' => 'title',
                'mo_ta' => '',
                'value' => $produce['title'],
                'tag' => 'input'
            ],
            'description' => [
                'title' => 'Mô tả',
                'name' => 'description',
                'mo_ta' => '',
                'value' => $produce['description'],
                'tag' => 'input'
            ],
            'image' => [
                'title' => 'Hình Ảnh',
                'name' => 'image',
                'mo_ta' => 'Nên sử dụng hình ảnh có chiều dài bằng chiều rộng',
                'value' => '',
                'type' => 'file',
                'tag' => 'input'
            ],
            'slug' => [
                'title' => 'Liên kết',
                'name' => 'slug',
                'mo_ta' => '',
                'value' => $produce['slug'],
                'tag' => 'input'
            ],
            'tags' => [
                'title' => 'Từ khóa',
                'name' => 'tags',
                'mo_ta' => '',
                'value' => $produce['tags'],
                'tag' => 'input'
            ],
            
        ];
        return view('auth.form', [
            'form' => $form_data,
            'route_form' => 'admin.manager.edit_produces',
            'route_data' => $produce['id'],
            'title' => 'CHỈNH SỬA: ' . $produce->title
        ]);

    }
    
    public function actioneditProduces(Request $request, $id)
    {
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'slug' => 'required',
            'tags' => 'required',
        ]);
        $produce = Produce::find($id);

        if($request->hasFile('image')) {
            $produce->image = $this->save_img($request->image);
        }
        $produce->fill($request->all());

        $produce->save();
        
        return redirect()->back()->with('thong_bao', 'Sửa thành công nội dung thanh menu');
    }

    public function getPostProduces($id)
    {
        $data_post = PostProduce::where('id_produce', $id)->get();
        $data_list = [];
        foreach ($data_post as $item) {
            $data_list[] = $item->Post;
        }
        return view('auth.table_menu', [
            'title' => 'TỔNG HỢP CÁC BÀI VIẾT : ' .Produce::find($id)->title,
            'route_create' => 'admin.manager.create_post_produces',
            'route_edit' => 'admin.manager.post.edit',
            'route_remove' => 'admin.manager.remove_postproduces',
            'data_route_create' => $id,
            'data_route_edit' => 'slug',
            'data' => $data_list,
            'table_colum' => [
                [
                    'name' => 'Tên Hiển Thị',
                    'id' => 'title'
                ],
                [
                    'name' => 'Hình Ảnh',
                    'id' => 'image',
                    'type' => 'file',
                    'mo_ta' => 'Nên sử dụng hình ảnh có chiều dài bằng chiều rộng'
                ],
                [
                    'name' => 'Từ Khóa',
                    'id' => 'tags',
                    'mo_ta' => 'Các từ cách nhau bởi dấu phẩy'
                ],
                [
                    'name' => 'Mô Tả',
                    'id' => 'description'
                ],
                [
                    'name' => 'Liên Kết',
                    'id' => 'slug',
                    'mo_ta' => 'Viết không dấu và Các từ cách nhau bởi dấu "-"'
                ]
            ]
        ]);
    }
    public function createPostProduces(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'slug' => 'required',
            'tags' => 'required',
        ]);

        $post_new = new Post;


        $post_new->fill($request->all());

        $post_new->id_user = Auth::user()->id;
        $post_new->content = 'Chưa có nội dung';
        $post_new->save();
        
        $post_produce = new PostProduce;

        $post_produce->id_produce = $id;
        $post_produce->id_post = $post_new->id;
        $post_produce->save();
        return redirect()->route('admin.manager.post.edit', $post_new->slug)->with('thong_bao', 'Tạo thành công sản phẩm mới');
    }
    public function actionRemovePostProduces($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $post_produce = PostProduce::where('id_post', $post->id)->first();

        $post_produce->delete();
        $post->delete();
        
        return redirect()->back()->with('thong_bao', 'Xóa thành công sản phẩm');
    }
    public function save_img($file)
    {
        $file_name = md5($this->RandomString()) . '.' . $file->getClientOriginalExtension();
        $file->move('upload', $file_name);
        return $file_name ;
    }
    function RandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            $randstring = $characters[rand(0, strlen($characters) - 1)];
        }
        return $randstring;
    }

    

}
