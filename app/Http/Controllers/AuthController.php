<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    public function actionHome()
    {
        $route = Auth::check() ? 'manager.index' : 'login';
        return redirect()->route('admin.' . $route);
    }
    public function actionLogin(Request $request)
    {
        $this->validate($request , [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $remember = $request->remember == 'on' ? true : false;
        
        Auth::attempt($request->only('email', 'password'), $remember);

        return redirect()->route('admin.home');
    }
    public function actionLogout()
    {
        Auth::logout();
        return redirect()->route('admin.home');
    }
}
