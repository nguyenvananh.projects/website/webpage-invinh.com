<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Produce;
use App\Models\Infomation;

class FrontendController extends Controller
{
    public function actionHome()
    {
        $post = Post::where('slug', Infomation::find(1)->link_post_index)->first();
        
        return view('frontend.index', [
            'post' => $post
        ]);
    }
    public function actionMap()
    {
        return view('frontend.map', [
            'title' => 'Bản đồ chỉ đường đến shop'
        ]);
    }
    public function actionPost($slug)
    {
        $post = Post::where('slug', $slug)->first();

        if($post){
            return view('frontend.post', [
                'post' => $post,
                'title' => $post->title,
                'lists_post' => Post::all(),
                'seo' => []
            ]);
        }else {
            return redirect()->route('index');
        }
    }
    public function actionProduce ($slug)
    {
        $produce = Produce::where('slug', $slug)->first();
        if($produce){
            return view('frontend.produce', [
                'produce' => $produce,
                'title' => $produce->title,
                'seo' => []
            ]);
        }else {
            return redirect()->route('index');
        }
    }
}
