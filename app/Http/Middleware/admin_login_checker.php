<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class admin_login_checker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $status)
    {
        $status = $status == 'no' ? false : true;
        
        if(Auth::check() == $status) {
            return $next($request);
        }
        else {
            return redirect()->route('admin.home');
        }
    }
}
