<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Infomation extends Model
{
    protected $fillable = [
        'title',
        'description',
        'keywords',
        'address',
        'coordinate',
        'email',
        'phone_numbers',
        'link_post_index',
        'banner'
    ];
}
