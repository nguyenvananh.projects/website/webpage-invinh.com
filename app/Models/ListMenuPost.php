<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListMenuPost extends Model
{
    protected $fillable = [
        'name', 
        'description',
        'route_name'
    ];
}
