<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostProduce extends Model
{
    public function Post()
    {
        return $this->belongsTo('App\Models\Post', 'id_post');
    }
    public function Produce()
    {
        return $this->belongsTo('App\Models\Produce', 'id_produce');
    }
}
