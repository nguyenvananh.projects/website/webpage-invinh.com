<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produce extends Model
{
    protected $fillable = [
        'title',
        'description',
        'slug',
        'tags'
    ];

    
    public function PostProduce()
    {
        return $this->hasMany('App\Models\PostProduce', 'id_produce');
    }
    public function Posts()
    {
        $data = [];
        foreach($this->PostProduce as $item) {
            $data[] = $item->Post;
        }
        return $data;
    }
}
