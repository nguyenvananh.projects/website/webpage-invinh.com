<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Infomation;
use App\Models\ListMenuPost;
use App\Models\Produce;
use App\Models\Support;
use App\Models\Slide;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share([
            'default_infomation' => Infomation::find(1),
            'list_menu_post' => ListMenuPost::all(),
            'list_produces' => Produce::all(),
            'list_supports' => Support::all(),
            'data_sliders' => Slide::all()
        ]);
    }
}
