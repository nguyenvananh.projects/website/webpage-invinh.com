<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostProducesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_produces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_produce')->unsigned();
            $table->integer('id_post')->unsigned();
            $table->timestamps();
            $table->foreign('id_post')->references('id')->on('posts');
            $table->foreign('id_produce')->references('id')->on('produces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_produces');
    }
}
