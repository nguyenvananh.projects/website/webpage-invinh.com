<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InfomationTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('infomations')->insert([
            'title' => 'Tiêu đề mặc định', 
            'description' => 'Mô tả mặc định',
            'keywords' => 'tukhoa1, tukhoa2, tukhoa3',
            'address' => 'Địa chỉ mặc định',
            'coordinate' => 'Tọa độ Google map',
            'email' => 'vidu@gmail.com',
            'phone_number' => '0123456789'
        ]);
    }
}
