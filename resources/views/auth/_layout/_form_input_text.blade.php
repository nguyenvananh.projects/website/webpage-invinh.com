<div class="form-group">
    <label for="input_text_{{ $name }}">{{ $title }}</label>
    <span>{!! $mo_ta !!}</span>
    <input name="{{ $name }}" type="{{ isset($type) ? $type : 'text' }}" class="form-control" id="input_text_{{ $name }}" placeholder="{{ $title }}" value="{{ $value }}">
</div>