<div class="form-group">
    <label for="input_text_{{ $name }}">{{ $title }}</label>
    <span>{!! $mo_ta !!}</span>
    
    <textarea name="{{ $name }}" class="form-control " id="textarea_{{ $name }}">{{ $value }}</textarea>

</div>
<script> CKEDITOR.replace( 'textarea_{{ $name }}', {
    filebrowserBrowseUrl: '{{ asset('vendor/ckfinder/ckfinder.html') }}',
    filebrowserImageBrowseUrl: '{{ asset('vendor/ckfinder/ckfinder.html?type=Images') }}',
    filebrowserFlashBrowseUrl: '{{ asset('vendor/ckfinder/ckfinder.html?type=Flash') }}',
    filebrowserUploadUrl: '{{ asset('vendor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
    filebrowserImageUploadUrl: '{{ asset('vendor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    filebrowserFlashUploadUrl: '{{ asset('vendor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
} );</script>