{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    @if($errors->any()) 
        @foreach($errors->all() as $error)
            toastr["error"]("{{ $error }}")
        @endforeach
        alert('Đã xuất hiện lỗi nhập dữ liệu')
    @endif
    @if(session('thong_bao'))
      toastr["{{ session('trang_thai') ? session('trang_thai') : 'success' }}"]("{{ session('thong_bao') }}")
    @endif
    toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "8000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    }
</script>
@stop