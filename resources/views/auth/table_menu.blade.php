{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
<div class="text-right" style="margin-bottom: 10px">
    <button data-toggle="modal" data-target="#CreateListMenu" class="btn btn-secondary"><i class="fa fa-plus"></i>  Thêm mới nội dung</button>
</div>
<table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
    <th scope="col">#</th>
      @foreach($table_colum as $colum)
      <th scope="col">{{ $colum['name'] }}</th>
      @endforeach
      <th scope="col">Hành Động</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data as $row)
    <tr>
      <td scope="col">{{ $row->id }}</td>
      @foreach($table_colum as $colum)
      <td scope="col">{{ $row[$colum['id']] }} @if(isset($colum['type']) && $colum['type'] == 'file') <a href="/upload/{{$row[$colum['id']]}}">[Xem]</a>@endif</td>
      @endforeach
      <td scope="col">
      @if(isset($data_view))
      <a href="{{ route(isset($data_view) ? $data_view  : 'home' , isset($data_route_edit) ? $row[$data_route_edit] : '') }}" class=" btn btn-sm btn-info"><i class="fa fa-eye"></i> Xem</a>
      @endif
      @if(isset($route_edit))
      <a href="{{ route(isset($route_edit) ? $route_edit  : 'home' , isset($data_route_edit) ? $row[$data_route_edit] : '') }}" class=" btn btn-sm btn-info"><i class="fa fa-edit"></i> SỬA</a>
      @endif
      <a href="{{ route(isset($route_remove) ? $route_remove  : 'home' , isset($data_route_edit) ? $row[$data_route_edit] : '') }}"  class=" btn btn-sm btn-danger" onclick="return confirm('Bạn có chắc xóa dữ liệu này không');"><i class="fa fa-ban"></i> XÓA</a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="CreateListMenu" tabindex="-1" role="dialog" aria-labelledby="CreateListMenuLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form enctype="multipart/form-data" action="{{ isset($data_route_create) ? route($route_create, $data_route_create) : route($route_create) }}" method="post" class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="CreateListMenuLabel">Thêm mới nội dung</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      @csrf
        @foreach($table_colum as $colum)
        @include('auth._layout._form_input_text', [
          'title' => $colum['name'],
          'mo_ta' => isset($colum['mo_ta']) ? $colum['mo_ta'] : '',
          'value' => '',
          'type' => isset($colum['type']) ? $colum['type'] : '',
          'name' => $colum['id']
        ])
        @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tạo mới</button>
      </div>
    </form>
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    @if($errors->any()) 
        @foreach($errors->all() as $error)
            toastr["error"]("{{ $error }}")
        @endforeach
        alert('Đã xuất hiện lỗi nhập dữ liệu')
    @endif
    @if(session('thong_bao'))
      toastr["{{ session('trang_thai') ? session('trang_thai') : 'success' }}"]("{{ session('thong_bao') }}")
    @endif

    toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "8000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    }
</script>
@stop