@extends('frontend.master')

@section('content')

@if($post)
<div class="sidebar-post-content" style="padding: 0;">
    <h3 class="sidebar-lg-title">{{ $post->title }}</h3>
</div>
<div class="sidebar-desc mb-50">
    <blockquote class="mtb-30"> <p>{{ $post->description }}</p></blockquote>
    {!! substr($post->content, 0, 2000) !!}
</div>
<div>
    <a href="{{ route('post_view', $post->slug) }}" class="btn btn-block btn-light">Xem chi tiết</a>
</div>
@else
<h1>Chưa thiết lập nội dung</h1>
@endif
@endsection