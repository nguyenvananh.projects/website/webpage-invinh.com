<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ isset($title) ? $title : $default_infomation->title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base href="{{ asset('') }}">

    <meta name="description" content="{{ isset($seo['description']) ? $seo['description'] : $default_infomation->description }}"/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ isset($seo['title']) ? $seo['title'] : $default_infomation->title }}" />
    <meta property="og:description" content="{{ isset($seo['description']) ? $seo['description'] : $default_infomation->description }}" />
    <meta property="og:url" content="https://invinh.vn/" />
    <meta property="og:site_name" content="{{ isset($seo['title']) ? $seo['title'] : $default_infomation->title }}" />
    <meta property="og:image" content="{{ isset($seo['image']) ? $seo['image'] : '/upload/' . $default_infomation->banner }}" />
    <meta property="og:image:secure_url" content="{{ isset($seo['image']) ? $seo['image'] : '/upload/' . $default_infomation->banner }}" />
    <meta property="og:image:width" content="400" />
    <meta property="og:image:height" content="260" />
    <meta name="twitter:description" content="{{ isset($seo['description']) ? $seo['description'] : $default_infomation->description }}" />
    <meta name="twitter:title" content="{{ isset($seo['title']) ? $seo['title'] : $default_infomation->title }}" />
    <meta name="twitter:image" content="{{ isset($seo['image']) ? $seo['image'] : '/upload/' . $default_infomation->banner }}" />
    <meta name="geo.region" content="VN" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="img/icon/favicon.png">
    <!-- Fontawesome css -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons css -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- linearicons css -->
    <link rel="stylesheet" href="css/linearicons.css">
    <!-- Nice select css -->
    <link rel="stylesheet" href="css/nice-select.css">
    <!-- Jquery fancybox css -->
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <!-- Jquery ui price slider css -->
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <!-- Meanmenu css -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- Nivo slider css -->
    <link rel="stylesheet" href="css/nivo-slider.css">
    <!-- Owl carousel css -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Custom css -->
    <link rel="stylesheet" href="css/default.css">
    <!-- Main css -->
    <link rel="stylesheet" href="style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- Modernizer js -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
</head>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!-- Main Wrapper Start Here -->
    <div class="wrapper">
        <!-- Banner Popup Start -->
        <div class="popup_banner">
            <span class="popup_off_banner">×</span>
            <div class="banner_popup_area">
                    <img src="{{ $default_infomation->banner ? $default_infomation->banner : 'img/banner/pop-banner.jpg' }}" alt="">
            </div>
        </div>
        <!-- Banner Popup End -->
        <!-- Main Header Area Start Here -->
        <header>
            <!-- Header Top Start Here -->
            <div class="header-top-area">
                <div class="container">
                    <!-- Header Top Start -->
                    <div class="header-top">
                        <ul></ul>
                        <ul style="width: 100%">
                            <li style="width: 100%"><marquee scrollamount="10" onMouseOver="this.stop();" onMouseOut="this.start();" style=" color: #FFF"><strong>Chào mừng bạn đến với: {{ $default_infomation->title }} - {{ $default_infomation->description }} - Liên hệ ngay với chúng tôi qua số điện thoại: {{ $default_infomation->phone_number }}</strong></marquee></li>
                        </ul>
                    </div>
                    <!-- Header Top End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Top End Here -->
            <!-- Header Middle Start Here -->
            <div class="header-middle ptb-15">
                <div class="container">
                    <div class="row align-items-center no-gutters">
                        <div class="col-lg-3 col-md-12">
                            <div class="logo mb-all-30">
                                <a href="index.html"><img style="width: 90%" src="http://dongphucline.com/wp-content/uploads/2018/07/logo-line.png" alt="logo-image"></a>
                            </div>
                        </div>
                        <!-- Categorie Search Box Start Here -->
                        <div class="col-lg-5 col-md-8 ml-auto mr-auto col-10">
                            <div class="categorie-search-box">
                                <form action="#">
                                    <input type="text" name="search" placeholder="Bạn cần tìm kiếm sản phẩm gì ?">
                                    <button><i class="lnr lnr-magnifier"></i></button>
                                </form>
                            </div>
                        </div>
                        <!-- Categorie Search Box End Here -->
                        <!-- Cart Box Start Here -->
                        <div class="col-lg-4 col-md-12">
                            <div class="cart-box mt-all-30">
                                <ul class="d-flex justify-content-lg-end justify-content-center align-items-center">
                                    <li><a href="{{ route('map') }}"><i style="color: #007b3e; font-size: 38px" class="fa fa-map-marker"></i><span class="my-cart"><span> <strong>Chỉ đường</strong> </span><span>Đến Shop</span></span></a></li>
                                    <li><a href="tel:{{ $default_infomation->phone_number }}"><i style="color: #007b3e; font-size: 38px" class="fa fa-phone-square"></i><span class="my-cart"><span> <strong>Gọi điện</strong> </span><span>Đặt hàng</span></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Cart Box End Here -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Middle End Here -->
            <!-- Header Bottom Start Here -->
            <div class="header-bottom  header-sticky">
                <div class="container">
                    <div class="row align-items-center">
                         <div class="col-xl-3 col-lg-4 col-md-6 vertical-menu d-none d-lg-block">
                            <span class="categorie-title">Danh mục sản phẩm </span>
                        </div>                       
                        <div class="col-xl-9 col-lg-8 col-md-12 ">
                            <nav class="d-none d-lg-block">
                                <ul class="header-bottom-list d-flex">
                                    <li><a href="{{ route('home') }}">home</a></li>
                                    @foreach($list_menu_post as $item_menu_post)
                                    <li><a href="{{ $item_menu_post->route_name }}">{{ $item_menu_post->name }}</a></li>
                                    @endforeach
                                </ul>
                            </nav>
                            <div class="mobile-menu d-block d-lg-none">
                                <nav>
                                    <ul>
                                        <li><a href="{{ route('home') }}">home</a></li>
                                        @foreach($list_menu_post as $item_menu_post)
                                        <li><a href="{{ $item_menu_post->route_name }}">{{ $item_menu_post->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Bottom End Here -->
            <!-- Mobile Vertical Menu Start Here -->
            <div class="container d-block d-lg-none">
                <div class="vertical-menu mt-30">
                    <span class="categorie-title mobile-categorei-menu">Danh mục sản phẩm</span>
                    <nav>  
                        <div id="cate-mobile-toggle" class="category-menu sidebar-menu sidbar-style mobile-categorei-menu-list menu-hidden ">
                            <ul>
                                @foreach($list_produces as $item_produce)
                                <li class="has-sub"><a href="#">{{ $item_produce->title }}</a>
                                    <ul class="category-sub">
                                        @foreach($item_produce->Posts() as $post)
                                        <li><a href="{{ route('post_view', $post->slug) }}">{{ $post->title }}</a></li>
                                        @endforeach
                                    </ul>
                                    <!-- category submenu end-->
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- category-menu-end -->
                    </nav>
                </div>
            </div>
            <!-- Mobile Vertical Menu Start End -->
        </header>
        <!-- Main Header Area End Here -->
        <!-- Categorie Menu & Slider Area Start Here -->
        <div class="main-page-banner pb-50 white-bg">
            <div class="container">
                <div class="row">
                    <!-- Vertical Menu Start Here -->
                    <div class="col-xl-3 col-lg-4 d-none d-lg-block">
                        <div class="vertical-menu mb-all-30">
                            <nav>
                                <ul class="vertical-menu-list">
                                    @foreach($list_produces as $item_produce)
                                    <li class=""><a href="{{ route('produce_view', $item_produce->slug) }}">{{ $item_produce->title }}<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <ul class="ht-dropdown mega-child">
                                            @foreach($item_produce->Posts() as $post)
                                            <li> <a href="{{ route('post_view', $post->slug) }}">(+) {{ $post->title }}</a></li>
                                            @endforeach
                                        </ul>
                                        <!-- category submenu end-->
                                    </li>
                                    @endforeach
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- Vertical Menu End Here -->
                    <!-- Slider Area Start Here -->
                    <div class="col-xl-9 col-lg-8 slider_box">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach($data_sliders as $stt_slider => $slider)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $stt_slider }}" class="{{ $stt_slider  == 0 ? 'active' : ''}}"></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @foreach($data_sliders as $stt_slider => $slider)
                            <div class="carousel-item {{ $stt_slider  == 0 ? 'active' : ''}}">
                                <img class="d-block w-100" src="/upload/{{ $slider->src }}" alt="{{ $stt_slider }} slide">
                            </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    </div>
                    <!-- Slider Area End Here -->
                </div>
                <!-- Row End -->
                <div class="row">
                <div class="container" style="margin-top: 20px">
                    <div class="breadcrumb">
                        <ul class="d-flex align-items-center">
                            <li><a href="{{ asset('') }}"><strong>Trang Chủ</strong></a></li>
                            <li class="active"><strong>{{ isset($title) ? $title : $default_infomation->title }}</strong></li>
                        </ul>
                    </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 order-2 order-lg-1">
                        <div class="single-sidebar latest-pro mb-30">
                            <h3 class="sidebar-title">Hỗ Trợ Trực Tuyến</h3>
                            <ul class="sidbar-style">
                                @foreach($list_supports as $account_sp)
                                    <li><a href="#"><img style="width: 30px; padding-right: 5px;" src="/img/icon/logo_{{ $account_sp->social }}.png" alt="">{{ $account_sp->name }} : {{ $account_sp->href }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="single-sidebar latest-pro mb-30">
                            <h3 class="sidebar-title">Từ Khóa Tìm Kiếm</h3>
                            <div class="sidbar-style">
                                <ul class="tag-list">
                                    @foreach(explode(',', $default_infomation->keywords) as $key)
                                    <li style="border: none"><a href="#">{{ $key }}</a></li>
                                    @endforeach
                                </ul>
                                </div>
                        </div>
                        <div class="single-sidebar latest-pro mb-30">
                            <h3 class="sidebar-title">Thống Kê Truy Cập</h3>
                            <div align="center" class="sidbar-style" style="padding: 15px;">
                                <a href='https://www.hit-counts.com/'>
                                    <img src='http://www.hit-counts.com/counter.php?t=MTQ0NTM4OA=='  alt='counts visit creation'>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 order-1 order-lg-2">
                        @yield('content')
                        
                    </div>
                </div>
            </div>
            <!-- Container End -->
        </div>
        <!-- Categorie Menu & Slider Area End Here -->
        

        <!-- Footer Area Start Here -->
        <footer class="off-white-bg2 bdr-top pt-sm-55">
            <!-- Footer Top Start -->
            <div class="footer-top">
                
                <!-- Container End -->
            </div>
            <!-- Footer Top End -->
            <!-- Footer Middle Start -->
            <div class="footer-middle text-center">
                <div class="container">
                    <div class="footer-middle-content pt-20 pb-30">
                            <ul class="social-footer">
                            </ul>
                    </div>
                </div>
                <!-- Container End -->
            </div>
            <!-- Footer Middle End -->
            <!-- Footer Bottom Start -->
            <div class="footer-bottom pb-30">
                <div class="container">

                     <div class="copyright-text text-center">                    
                        <h5>Copyright © 2020 - <a target="_blank" href="{{ route('home') }}">{{ $default_infomation->title }}</a></h5>
                     </div>
                </div>
                <!-- Container End -->
            </div>
            <!-- Footer Bottom End -->
        </footer>
        <!-- Footer Area End Here -->
    </div>
    <!-- Main Wrapper End Here -->


    
    <!-- jquery 3.2.1 -->
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <!-- Countdown js -->
    <script src="js/jquery.countdown.min.js"></script>
    <!-- Mobile menu js -->
    <script src="js/jquery.meanmenu.min.js"></script>
    <!-- ScrollUp js -->
    <script src="js/jquery.scrollUp.js"></script>
    <!-- Nivo slider js -->
    <script src="js/jquery.nivo.slider.js"></script>
    <!-- Fancybox js -->
    <script src="js/jquery.fancybox.min.js"></script>
    <!-- Jquery nice select js -->
    <script src="js/jquery.nice-select.min.js"></script>
    <!-- Jquery ui price slider js -->
    <script src="js/jquery-ui.min.js"></script>
    <!-- Owl carousel -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- Bootstrap popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugin js -->
    <script src="js/plugins.js"></script>
    <!-- Main activaion js -->
    <script src="js/main.js"></script>
</body>

</html>