@extends('frontend.master')

@section('content')
    <div class="sidebar-post-content" style="padding: 0;">
        <h3 class="sidebar-lg-title">{{ $post->title }}</h3>
    </div>
    <div class="sidebar-desc mb-50">
        <blockquote class="mtb-30"> <p>{{ $post->description }}</p></blockquote>
        {!! $post->content !!}
    </div>
    
    
    
        <!-- Like Products Area Start Here -->
        <div class="like-product ptb-sm-55 off-white-bg" style="padding: 20px 0;">
            <div class="container">
                <div class="like-product-area"> 
                    <h2 class="section-ttitle2 mb-30">TOTAL NEW POSTS</h2>
                    <!-- Like Product Activation Start Here -->
                    <div class="like-pro-active owl-carousel">
                        @foreach($lists_post as $post_item)
                        <!-- Single Product Start -->
                        <div class="single-product">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="{{ route('post_view', $post_item->slug) }}">
                                    <img width="160px" height="160px" class="primary-img" src="{{ $post_item->image ? '/upload/' . $post_item->image  : 'img/products/no-image.png' }}" alt="single-product">
                                    <img width="160px" height="160px"  class="secondary-img" src="{{ $post_item->image ? '/upload/' . $post_item->image : 'img/products/no-image.png' }}" alt="single-product">
                                </a>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content">
                                <div class="pro-info">
                                    <h4><a href="{{ route('post_view', $post_item->slug) }}">{{ $post_item->title }}</a></h4>
                                </div>
                                <div class="pro-actions">
                                    <div class="actions-primary">
                                        <a href="{{ route('post_view', $post_item->slug) }}" title="{{ $post_item->title }}">Xem chi tiết</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End --> 
                        @endforeach 
                    </div>                    
                    <!-- Like Product Activation End Here -->
                </div>
                <!-- main-product-tab-area-->
            </div>
            <!-- Container End -->
        </div>
        <!-- Like Products Area End Here -->   
@endsection