@extends('frontend.master')

@section('content')
                        <!-- Grid & List View End -->
                        <div class="main-categorie mb-all-40">
                            <!-- Grid & List Main Area End -->
                            <div class="tab-content fix">
                                <div id="grid-view" class="tab-pane fade show active">
                                    <div class="row">
                                        @foreach($produce->Posts() as $post)
                                        <!-- Single Product Start -->
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                            <div class="single-product">
                                                <!-- Product Image Start -->
                                                <div class="pro-img">
                                                    <a href="product.html">
                                                        <img class="primary-img" src="{{ $post->image ? '/upload/' .  $post->image : 'img/products/no-image.png' }}" alt="{{ $post->title }}">
                                                        <img class="secondary-img" src="{{ $post->image ? '/upload/' . $post->image : 'img/products/no-image.png' }}" alt="{{ $post->title }}">
                                                    </a>
                                                </div>
                                                <!-- Product Image End -->
                                                <!-- Product Content Start -->
                                                <div class="pro-content">
                                                    <div class="pro-info">
                                                        <h4><a href="{{ route('post_view', $post->slug) }}">{{ $post->title }}</a></h4>
                                                        <p><span>{{ $post->description }}</span></p>
                                                    </div>
                                                    <div class="pro-actions">
                                                        <div class="actions-primary">
                                                            <p>{{ $post->title }}</p>
                                                            <a href="{{ route('post_view', $post->slug) }}" title="{{ $post->title }}" data-original-title="{{ $post->title }}">Xem chi tiết</a>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Product Content End -->
                                            </div>
                                        </div>
                                        <!-- Single Product End -->
                                        @endforeach
                                </div>
                            </div>
                            <!-- Grid & List Main Area End -->
                        </div>
@endsection