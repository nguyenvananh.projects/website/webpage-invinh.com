<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', ['as' => 'home', 'uses' => 'FrontendController@actionHome']);
Route::get('index.html', ['as' => 'index', 'uses' => 'FrontendController@actionHome']);
Route::get('/map.html', ['as' => 'map', 'uses' => 'FrontendController@actionMap']);
Route::get('posts/{slug}.html', ['as' => 'post_view', 'uses' => 'FrontendController@actionPost']);
Route::get('produce/{slug}.html', ['as' => 'produce_view', 'uses' => 'FrontendController@actionProduce']);






Route::group(['prefix' => 'admin_setup_page', 'as' => 'admin.'], function() {
    Route::get('/', ['as' => 'home', 'uses' => 'AuthController@actionHome']);
    
    Route::group(['middleware' => 'login:no'], function() {
        Route::get('login.html', function () {
            return view('auth.login');
        })->name('login');
    
        Route::post('login.html', ['as' => 'login', 'uses' => 'AuthController@actionLogin']);
    });

    Route::group(['prefix' => 'manager','middleware' => 'login:yes', 'as' => 'manager.'], function() {
        Route::post('logout.html', ['as' => 'logout', 'uses' => 'AuthController@actionLogout']);
        Route::get('index.html', ['as' => 'index', 'uses' => 'Admin\HomeController@getHome']);
        Route::post('index.html', ['as' => 'index', 'uses' => 'Admin\HomeController@actionEditInfomation']);
    
        
        Route::get('list-post-menu.html', ['as' => 'list_post_menu', 'uses' => 'Admin\HomeController@getListPostMenu']);
        Route::post('create-list-post-menu.html', ['as' => 'create_list_post_menu', 'uses' => 'Admin\HomeController@createListPostMenu']);
        Route::get('list-post-menu/{slug}/remove.html', ['as' => 'remove_list_post_menu', 'uses' => 'Admin\HomeController@removeListPostMenu']);

        Route::get('posts/{slug}.html', ['as' => 'post.edit', 'uses' => 'Admin\HomeController@getEditSlug']);
        Route::post('posts/{slug}.html', ['as' => 'post.edit', 'uses' => 'Admin\HomeController@actionEditSlug']);
        Route::get('posts/{slug}/remove.html', ['as' => 'post.remove', 'uses' => 'Admin\HomeController@actionRemove']);
        
        Route::get('posts.html', ['as' => 'posts', 'uses' => 'Admin\HomeController@getPosts']);
        Route::post('create-posts.html', ['as' => 'create_posts', 'uses' => 'Admin\HomeController@actionCreatePosts']);

        
        Route::get('supports.html', ['as' => 'supports', 'uses' => 'Admin\HomeController@getSupport']);
        Route::post('create-supports.html', ['as' => 'create_support', 'uses' => 'Admin\HomeController@createSupport']);
        Route::get('remove-supports-{id}.html', ['as' => 'remove_support', 'uses' => 'Admin\HomeController@actionRemoveSupport']);
        

        Route::get('produces.html', ['as' => 'produces', 'uses' => 'Admin\HomeController@getProduces']);
        Route::post('create-produces.html', ['as' => 'create_produces', 'uses' => 'Admin\HomeController@actionCreateProduces']);
        Route::get('remove-produces-{id}.html', ['as' => 'remove_produces', 'uses' => 'Admin\HomeController@actionRemoveproduces']);

        
        Route::get('slides.html', ['as' => 'slides', 'uses' => 'Admin\HomeController@getSlides']);
        Route::post('create-slides.html', ['as' => 'create_slides', 'uses' => 'Admin\HomeController@actionCreateSlides']);
        Route::get('remove-slides-{id}.html', ['as' => 'remove_slides', 'uses' => 'Admin\HomeController@actionRemoveSlidess']);

        Route::get('edit-produces-{id}.html', ['as' => 'edit_produces', 'uses' => 'Admin\HomeController@editProduces']);
        Route::post('edit-produces-{id}.html', ['as' => 'edit_produces', 'uses' => 'Admin\HomeController@actioneditProduces']);
        
        Route::get('produces/post-{id}.html', ['as' => 'post_produces', 'uses' => 'Admin\HomeController@getPostProduces']);
        Route::post('produces/create-post-{slug}.html', ['as' => 'create_post_produces', 'uses' => 'Admin\HomeController@createPostProduces']);
        Route::get('remove--post-produces-{slug}.html', ['as' => 'remove_postproduces', 'uses' => 'Admin\HomeController@actionRemovePostProduces']);

        Route::post('login.html', ['as' => 'login', 'uses' => 'AuthController@actionLogin']);
    });
});